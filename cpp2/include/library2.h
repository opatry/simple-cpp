#pragma once

namespace acme2
{

class Zorg
{
public:
  Zorg(int val = 0);

  int baz() const;
private:
  int mVal;
};

}
