#include "library2.h"

using namespace acme2;

Zorg::Zorg(int val)
: mVal(val)
{
}

int Zorg::baz() const
{
  return mVal;
}
