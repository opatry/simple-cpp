#include <jni.h>

#include <library.h>

#ifdef __cplusplus
extern "C" {
#endif

jint JNICALL Java_org_acme_library_LibraryNatives_foo(JNIEnv *env, jobject /* this */, jint val) {
  acme::Foo foo(val);
  return foo.bar();
}

#ifdef __cplusplus
}
#endif
