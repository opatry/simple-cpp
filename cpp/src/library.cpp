#include "library.h"

using namespace acme;

Foo::Foo(int val)
: mVal(val)
, mZorg(2)
{
}

int Foo::bar() const
{
  return mVal * mZorg.baz();
}
