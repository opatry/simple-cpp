#pragma once

#include <library2.h>

namespace acme
{

class Foo
{
public:
  Foo(int val);

  int bar() const;
private:
  int mVal;
  acme2::Zorg mZorg;
};

}
