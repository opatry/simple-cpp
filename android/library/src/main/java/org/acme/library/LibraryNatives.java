package org.acme.library;

public final class LibraryNatives
{
  private LibraryNatives(){}

  public static native int foo(int val);

}
