package org.acme.simplecpp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.acme.library.LibraryNatives

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sample_text.text = "Hello ${LibraryNatives.foo(21)}"
        sample_text2.text = "Hello ${LibraryNatives.foo(42)}"
    }

    companion object {

        init {
            System.loadLibrary("c++_shared")
            System.loadLibrary("mylibrary")
        }
    }
}
